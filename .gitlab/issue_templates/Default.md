# Idea proposal 💡

## Checks

**NOTE**: The below check boxes **must be** checked before the accompanying
idea will be considered.

- [ ] I have checked that the idea is not directly tied to a specific project
  For example: "Show label icons in the package overview web page" must be a
  feature request in the ArchWeb repository
- [ ] I have carefully checked this idea is not already covered by any [open or
  closed ideas](https://gitlab.archlinux.org/archlinux/ideas/-/issues/?state=all).
- [ ] I understand that I hold no copyright claims and that this idea can be
  adapted and used by Arch Linux in any arbitrary shape or form.

## Summary

<!--
Briefly summarize **what** the idea is about.
Note: Verbose details should be added to the specification section.
-->

## Motivation

<!--
Explain the reason behind this idea and **why** this should be picked up.
-->

## Specification

<!--
A more verbose description about **what** should be done.
Optional: Include any thoughts about **how** it should be done.
-->
