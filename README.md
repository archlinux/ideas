Arch Linux Ideas 💡
==================

What is this about
------------------

This is an incubating idea pool useful to keep track of early stage suggestions
and brainstorming before they get picked up and implemented eventually.

Who can file an idea
--------------------

Anyone can file an idea.

Simply sign in to our GitLab instance through the Arch Linux SSO and [submit an
idea](https://gitlab.archlinux.org/archlinux/ideas/-/issues/new). The submission
is explicitly not limited to our staff, but open to the entire community.

When should I file an idea
--------------------------

If you have an idea for something that does not belong to a specific existing
project and is not already covered by any [open or closed
ideas](https://gitlab.archlinux.org/archlinux/ideas/-/issues/?state=all).

An idea is much more likely to get picked up if it begins life as a discussion,
and collaborators are encouraged to start off by talking to other project
members before submitting an idea. This can also help gauge early support for
the feature and work out any obvious issues or design decisions.

This idea pool is not a replacement for [RFCs](https://gitlab.archlinux.org/archlinux/rfcs)
but an earlier stage to collect raw ideas from brainstorming before they evolve
into an actual tool or RFC. The primary scope of this collection is to avoid
losing great ideas in the void because we forgot about them when only mentioned
during an IRC conversation or on a mailing list thread.

If you have new thoughts on a rejected idea, please use the discussion section
to provide information that could potentially lead to the issue being reopened.
Refer to the "Discussing an idea" section for more details on how to go about
this.

Proposing an idea
-----------------

Please put a lot of thought into your writing. A great idea explains and
justifies its motivations and carefully considers all drawbacks and
alternatives. Show the reader what the problems are and how the proposal
concretely and practically addresses them.

Discussing an idea
------------------

Discussion about an idea should happen in the opened issue on Arch Linux's
GitLab. If discussions happen outside the GitLab thread, it is best to link and
summarize them in the idea thread to make the conversations easier to track.

While discussing an idea, it is everyone's responsibility to work towards
filling it with details and conclusions so they become ready to get picked up
into a tool or [RFC](https://gitlab.archlinux.org/archlinux/rfcs). When you
make a post to the idea thread, focus on the contents and implications of the
idea only.

Here are some guidelines:

- **Think about these important questions:**
   - Do you agree with the proposal in its current form?
   - Are concerns and questions adequately addressed?
   - Do you have suggestions for how the idea can be revised or expanded?
- **Do not be shy:** Even if you are not an active Arch Linux contributor, or
  if you do not feel "qualified" to discuss the topic, please feel free to
  directly state your opinion on an idea, especially if it affects you in some
  way.
- **Stay on topic:** Starting or continuing tangential or off-topic discussions
  is disrespectful to the idea author and is strongly discouraged. Such
  discussions often dilute the thread and make it hard to identify what the
  community's feedback is. The purpose of the idea thread is not principally to
  reach a conclusion, but instead to challenge an idea and fill it with
  details. Any commentary that does not contribute to that is damaging to the
  idea's evolution.
- **Civility and respect:** Like all other discussions that happen in the Arch
  Linux community, issue discussions are governed by our [Code of
  Conduct](https://terms.archlinux.org/docs/code-of-conduct/).
  Commenting on an idea should be done with respect and empathy for the hard
  work that the author has put into it.
- **Avoid scope creep:** You should make suggestions on how the idea can be
  revised or expanded, but be careful not to expand it too much and develop it
  into something far more ambitious than was originally intended.

License
-------

All contributions are licensed by their respective authors under the terms of
the [CC-BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/legalcode).
